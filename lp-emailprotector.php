<?php
/*
Description: Provides a set of functions to display protected emails, and mailto links, in WP templates
Author: Loïc Pennamen

TODO: Allow adding subject to mailto link
TODO: Allow wrapping separate part of the email with spans for design
TODO: Add shortcode for une in WP content
*/

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
	exit;

// The class handles all the stuff
include_once 'LPEmailProtector.class.php';

// Init plugin
function lp_emailprotector_init(){

	// Add JS
	$jsDir = get_template_directory_uri() . '/vendor/loicpennamen/wpemailprotector/js/';
	wp_register_script('lp.emailprotector.js', $jsDir . 'lp.emailprotector.js');
	wp_enqueue_script('lp.emailprotector.js');

}

// Shorthand to get encoded e-mail as string
function lp_emailprotector($email, $format = 'link'){

	// Init the object
	$emailProtector = new LPEmailProtector();

	// Return the HTML/JS script
	if($format == 'link')
		return $emailProtector->getLink($email);

	// TODO: other possible formats?
	return null;
}

// Init plugin
add_action('init', 'lp_emailprotector_init');
