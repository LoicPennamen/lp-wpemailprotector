## Wordpress e-mail protector

Protects e-mails from spam in a HTML document by both server and client-side encryption. 
Designed for personal use in custom wordpress themes, with composer.