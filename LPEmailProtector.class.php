<?php

class LPEmailProtector{

	// ////
	// PUBLIC METHODS

	public function __construct(){
	}

	// Get HTML script tag for link to email
	public function getLink($email){

		// JS script:
		$json = $this->toJson($email);
		$js = 'LP.emailProtector.getLink(' . $json . ')';
		
		return $this->wrapJs($js);
	}

	// Wrap string in javascript HTML tag
	private function wrapJs($str){
		return '<script type="text/javascript">' . $str . '</script>';
	}

	// Get JSON data from email string
	private function toJson($email){
		return json_encode( $this->splitEmail($email) );
	}

	// Split email string to array of unclear data unusable by mean spamming bots
	private function splitEmail($email){
		$tmp = explode('@', $email);
		$data[] = $tmp[0];
		$data[] = substr($tmp[1], 0, strrpos($tmp[1], '.'));
		$data[] = substr($tmp[1], strrpos($tmp[1], '.'));
		
		// ASCII encoding of characters
		for ($i = 0; $i < sizeof($data); $i++)
			$data[$i] = $this->encodeStr($data[$i]);

		return $data;
	}

	// Encode a string in ASCII
	private function encodeStr($str){
		$output = '';
		for ($i = 0; $i < strlen($str); $i++)
			$output .= '&#'.ord($str[$i]).';';
		return $output;
	}
}